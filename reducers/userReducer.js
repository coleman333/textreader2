import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import userAction from '../actions/userAction';
import {createReducer} from 'redux-act';

const initialState = {
    error: {},
    user: {},
    allUsers: [],
    resParseText: {}
};

export default createReducer({
    [userAction.getTestUsers.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.getTestUsers.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.allUsers = payload.response.data;
        // console.log('this is from react native', newState.allUsers);
        return newState;
    },
    [userAction.getTestUsers.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

    [userAction.login.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.login.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        if (payload.response.data.user[0]) {
            newState.user = payload.response.data.user[0];
        }
        else {
            newState.user = {}
        }
        console.log('LOGIN OK', newState.user);
        return newState;
    },
    [userAction.login.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

    [userAction.readText.request]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert('READ TEXT ', newState.resParseText.text);
        return newState;
    },
    [userAction.readText.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        const resParseText = get(payload, 'response.data.resParseText');

        if (resParseText) {
            alert(`this is reducer read text: ${resParseText.text}`);
            newState.resParseText = resParseText;
        } else {
            newState.resParseText.text = '...'
        }
        return newState;
    },
    [userAction.readText.error]: (state, payload) => {
        const newState = cloneDeep(state);
        alert(`READ TEXT NOT OK ${newState.resParseText.text}`);
        return newState;
    },


}, initialState);
