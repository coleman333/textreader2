import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    TouchableHighlight,
    TouchableWithoutFeedback,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import NotificationWrapper from './NotificationWrapper';
import BadInstagramCloneApp from './BadInstagramCloneApp';

import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


import {StackActions, NavigationActions} from 'react-navigation';

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'


class LaunchTyping extends React.Component {
    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);
    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false,
        text: ''
    };

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }

    // spring = new Animated.Value(0);

    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
        this.setState({
            text: this.props.resParseText.text,
            companyName: this.props.resParseText.companyName,
            formNumber: this.props.resParseText.formNumber
        })
    }

    componentWillUnmount() {
        // this.keyboardDidShowListener.remove();
        // this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        //     this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        //     this.setState({normalHeight: Dimensions.get('window').height});
        //     this.setState({keyboardHeight: e.endCoordinates.height});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    _keyboardDidHide() {
        //     this.setState({toScroll: false});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        //
    }

    netsAuthorization() {
        Animated.timing(
            this._animValue2,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: false})
    }

    authPanelDisAnimate() {
        console.log('Test press');

        Animated.timing(
            this._animValue2,
            {
                toValue: 0,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: true})
    }

    handlePressIn() {
        Animated.spring(this.animatedValue1, {
            toValue: 0.5
        }).start()
    }

    handlePressOut() {
        Animated.spring(this.animatedValue1, {
            duration: 2000,
            toValue: 1,
            friction: 4,
            tension: 40
        }).start()
    }

    readText() {
        // this.props.userAction.readText() //because of it has no arguments passed through.
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;

        navigate('BadInstagramCloneApp');
    }

    confirmText(){
        this.props.navigation.navigate('LaunchTyping');
    }

    render() {

        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],

        })
        const animatedStyle = {transform: [{scale: this.animatedValue1}]}
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'gray',
                height: "100%"
            }}>
                <View style={{flex:8,width:'100%',justifyContent:'center',alignItems:'center'}}>
                    {/*<KeyboardAvoidingView enabled behavior="padding" style={{flex:1}}>*/}
                    {/*<TouchableWithoutFeedback onPressOut={this.authPanelDisAnimate.bind(this)}>*/}
                    <ScrollView
                        style={{width:'90%'}}
                        // style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}
                    >
                        <View style={{flexDirection:'row'}}>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Номер Документа</Text>
                            </View>

                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(text) => this.setState({text})}
                                value={this.state.text}
                                style={styles.textInput}
                            />
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>Поставщик</Text>
                            </View>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(text) => this.setState({text})}
                                value={this.state.text}
                                style={styles.textInput}
                            />
                        </View>



                    </ScrollView>
                </View>
                <View style={{flex:1}}>
                    <TouchableOpacity onPress={this.confirmText.bind(this)}
                        // onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}
                                      style={{
                                          borderWidth: 1,
                                          // borderColor:'rgba(0,0,0,0.2)',
                                          borderColor: 'black',
                                          alignItems: 'center',
                                          justifyContent: 'center',
                                          width: 200,
                                          height: 50,
                                          backgroundColor: 'gray',
                                          borderRadius: 50,
                                      }}
                    ><Text>Подтвердить</Text>
                    </TouchableOpacity>
                </View>

                {/*</KeyboardAvoidingView>*/}

            </View>


        );
    }
}

const styles =StyleSheet.create({
    ContainerLabel:{
        marginTop:10,
        // height: 50,
        width:'100%',
        // backgroundColor: '#eee',
        // borderWidth:1,
        // borderRadius:25,
        justifyContent: 'center',
    },
    textLabel:{
        textAlign:'center',
        alignItems:'center',
    },
    textInput:{
        borderColor:'black',
        marginTop:10,
        height: 50,
        width:'100%',
        backgroundColor: '#eee',
        borderWidth:1,
        borderRadius:25,
        justifyContent: 'center',
        textAlign: "center"
    }
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        // users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        // user: state.userReducer.login,
        resParseText: state.userReducer.resParseText

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LaunchTyping);
